#!/usr/bin/env python3

import argparse
import os
import sys
from pathlib import Path
import subprocess
import textwrap
import re

APP_DESCRIPTION=textwrap.dedent("""
SmartQML application bundler. Bundles QtCreator project into AppImage.
Requirements:
  - AppImage executable
  - LinuxDeployQt executable""")

FHS_PATH_HELP=textwrap.dedent("""Path to Filesystem Hierarchy Standard directory structure.
Example FHS structure:
└── usr
    ├── bin
    │   └── your_app
    ├── lib
    └── share
        ├── applications
        │   └── your_app.desktop
        └── icons
            └── <theme>
                └── <resolution> 
                    └── apps 
                        └── your_app.png""")

APPLICATION_PATH_HELP=textwrap.dedent("""Path to application executable or .desktop file.
Example minimal .desktop file:

[Desktop Entry]
Type=Application
Name=Amazing Qt App
Comment=The best Qt Application Ever
Exec=your_app
Icon=your_app
""")

LINUXDEPLOYQT_EXECUTABLE_DEFAULTS = ['linuxdeployqt', 'linuxdeployqt-continuous-x86_64.AppImage']

def existing_path(arg_path):
    p = Path(arg_path)
    if p.exists() and p.is_dir():
        return p
    else:
        msg = "{0!r} is not valid FHS path.".format(arg_path)
        raise argparse.ArgumentTypeError(msg)

def parse_arguments():
    parser = argparse.ArgumentParser(description=APP_DESCRIPTION, formatter_class=argparse.RawTextHelpFormatter)

    # Positional args
    parser.add_argument('filesystem_path', metavar='FHS-PATH', type=existing_path,
            help=FHS_PATH_HELP)
    parser.add_argument('application_path', metavar='APPLICATION', type=argparse.FileType('r'),
            help=APPLICATION_PATH_HELP)

    # Optional arguments
    parser.add_argument('-c', '--cache', action='store_true', 
            help="Do not replace existing files in FHS.")
    parser.add_argument('--no-appimage', action='store_true',
            help="Do not create appimage. Just get all necessary libraries, files, etc.")
    parser.add_argument('--bundle-qt-libs-only', action='store_true',
            help="Bundle qt libraries only.")
    parser.add_argument('--exclude-libs', type=str,
            help="List of libraries which should be excluded, separated by comam.")
    parser.add_argument('--ignore-glob', type=str, nargs=1,
            help="Glob pattern relative to FHS to ignore when searching for libraries.")
    parser.add_argument('--executable', type=argparse.FileType('r'), nargs="+",
            help="Additional executables to bundle.")
    parser.add_argument('--extra-plugins', type=str, nargs="+",
            help="List of extra plugins which should be deployed, separated by comma.")
    parser.add_argument('--no-copy-copyright-files', action='store_true',
            help="Skip deployment of copyright files.")
    parser.add_argument('--no-plugins', action='store_true',
            help="Skip plugin deployment.")
    parser.add_argument('--no-strip', action='store_true',
            help="Don't run 'strip' on the binaries.")
    parser.add_argument('--no-translations', action='store_true',
            help="Skip deployment of translations.")
    parser.add_argument('--qmake', type=argparse.FileType('r'), 
            help="Path to the qmake executable to use.")
    parser.add_argument('--qmldir', type=existing_path,
            help="Scan for QML imports in the given path.")
    parser.add_argument('-v', '--verbose', type=int, choices=[0, 1, 2, 3], default=0,
            help="0 = no output, 1 = error/warning (default), 2 = normal, 3 = debug.")
    parser.add_argument('--linuxdeployqt', nargs=1, type=str, 
            help="Path to LinuxDeployQt executable to use. Defaults: \n - linuxdeployqt,\n - linuxdeployqt-continuous-x86_64.AppImage")
    parser.add_argument('-s', '--script', type=argparse.FileType('r'),
            help="FHS preparation script.")
    parser.add_argument('-l', '--ldscript', type=argparse.FileType('r'),
            help="LD_LIBRARY_PATH preparation script. Will get application executable \npath as first argument.")

    return parser.parse_args()

def main():
    args = parse_arguments()

    if args.script:
        try:
            retcode = subprocess.call([args.script.name, 
                args.filesystem_path, 
                args.application_path.name])
            if retcode != 0:
                print("Script terminated with code: {}".format(retcode), file=sys.stderr)
                sys.exit(2) # Exit with error
        except OSError as e:
            print("Script execution failed:", e, file=sys.stderr)
            sys.exit(1) # exit with error

    ld_library_path = ""

    if args.ldscript:
        try:
            ld_library_path = str(subprocess.check_output([args.ldscript.name,
                args.application_path.name]).decode('utf-8')).rstrip()
        except subprocess.CalledProcessError as e:
            print("LD_LIBRARY_PATH processing script returned with code:", e.returncode, file=sys.stderr)
            sys.exit(1)


    commandArgs = []

    if not args.cache:
        commandArgs.append("-always-overwrite")

    if not args.no_appimage:
        commandArgs.append("-appimage")

    if not args.bundle_qt_libs_only:
        commandArgs.append("-bundle-non-qt-libs")

    if args.exclude_libs:
        commandArgs.append("-exclude-libs={}".format(args.exclude_libs))

    if args.ignore_glob:
        commandArgs.append("-ignore-glob={}".format(args.ignore_glob))

    if args.executable:
        commandArgs.extend(list(map(lambda e: "-executable={}".format(e), args.executable)))

    if args.extra_plugins:
        commandArgs.append("-extra-plugins={}".format(args.extra_plugins))

    if args.no_copy_copyright_files:
        commandArgs.append("-no-copy-copyright-files")

    if args.no_plugins:
        commandArgs.append("-no-plugins")

    if args.no_strip:
        commandArgs.append("-no-strip")

    if args.no_translations:
        commandArgs.append("-no-translations")

    if args.qmake:
        commandArgs.append("-qmake={}".format(args.qmake))

    if args.qmldir:
        commandArgs.append("-qmldir={}".format(args.qmldir))

    commandArgs.append("-verbose={}".format(args.verbose))

    executablesToTry = []

    if args.linuxdeployqt:
        executablesToTry = [args.linuxdeployqt]

    executablesToTry += LINUXDEPLOYQT_EXECUTABLE_DEFAULTS

    try:
        ld_library_path = str(ld_library_path) + ":" + str(os.environ['LD_LIBRARY_PATH'].decode('utf-8'))
    except KeyError as e:
        pass

    ld_library_path = ld_library_path.rstrip()

    lastException = None

    for executable in executablesToTry:
        try:
            my_env = os.environ.copy()
            my_env['LD_LIBRARY_PATH'] = str(ld_library_path)
            my_env['ARCH'] = "x86_64"

            retcode = subprocess.call([executable, args.filesystem_path / os.path.basename(args.application_path.name)] + commandArgs, env=my_env)
            if retcode != 0:
                print(executable, "terminated with code:", retcode, file=sys.stderr)
                exit(3)
            
            lastException = None
            break
        except OSError as e:
            lastException = e

    if lastException is not None:
        print("LinuxDeployQt execution error:", lastException, file=sys.stderr)
        exit(4)

if __name__ == '__main__':
    main()
