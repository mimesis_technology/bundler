#!/usr/bin/env bash

# Destination directory have to be passed as first argument of script.
# Application binary path have to be passed as second argument.

Log () {
  echo "LOG: $@"
}

Error () {
  echo "ERROR: $@" 1>&2
}

if [ -z "$1" ]; then
    Error "No filesystem path provided."
    exit 1
fi

if [ -z "$2" ]; then
    Error "No binary path provided."
    exit 1
fi

if ! [ -d "$1" ]; then
    Error "Destination path is not valid directory"
    exit 2
fi

if ! [ -x "$2" ]  || ! [ -r "$2" ]; then
    Error "Application executable path is not valid"
    exit 2
fi

# Fail on any command fail.
set -e

WORKDIR="$1"
APPLICATION_PATH="$2"

Log "WORKDIR=$WORKDIR"
Log "APPLICATION_PATH=$APPLICATION_PATH"

# Clean workdir.
rm -rf "$WORKDIR"/* | true

# Prepare structure.
LIB_DIR=usr/lib
BIN_DIR=usr/bin
APPLICATIONS_DIR=
ICONS_DIR=usr/share/icons/hicolor
mkdir -p "$WORKDIR"/usr/lib
mkdir -p "$WORKDIR"/usr/bin
mkdir -p "$WORKDIR"/usr/share/applications
mkdir -p "$WORKDIR"/usr/share/icons/hicolor/

# Copy files.
Log "Copying all QML modules."
QT_INSTALL_QML=`qmake -query | grep -i QT_INSTALL_QML | cut -d: -f2 -`
mkdir -p "$WORKDIR/qml"
cp -r "$QT_INSTALL_QML" "$WORKDIR"

copy_ld () {
	local lib_name="$1"
	local dest_dir="$2"
	ldconfig -p | grep -i "$lib_name" | awk '{ print $NF }' | xargs -i -n1 cp "{}" "$dest_dir"
}


Log "Copying libs."

WORK_LIB_DIR="$WORKDIR/$LIB_DIR/"

LIBS_TO_COPY=( 
	"libnss3.so"
	"libnssutil3.so"
	"libsmime3.so"
	"libssl3.so"
	"libfreebl3.so"
	"libfreeblpriv3.so"
	"libnssckbi.so"
	"libnssdbm3.so"
	"libnsssysinit.so"
	"libsoftokn3.so"
	"libnsl"
	"libcrypto"
	"libssl.so"
	)

for lib_name in "${LIBS_TO_COPY[@]}"; do
	Log "Copying '$lib_name' into '$WORK_LIB_DIR'"
	copy_ld "$lib_name" "$WORK_LIB_DIR" | true
done

Log "Copying application."
cp "$APPLICATION_PATH" "$WORKDIR"


Log "Preparing desktop file."

APP_BASENAME=`basename "$APPLICATION_PATH"`

cat <<- EOF > "$WORKDIR/$APP_BASENAME.desktop"
[Desktop Entry]
Type=Application
Name=$APP_BASENAME
Exec=$APP_BASENAME
Icon=$APP_BASENAME
EOF

# TODO: Podaj ścieżkę do obrazu aplikacji w formacjie svg lub png: 32x32, 64x64, 128x128, 256x256
cp ~/Pictures/Application.png "$WORKDIR/$APP_BASENAME.png"

Log "FHS prepared."
