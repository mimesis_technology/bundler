#!/usr/bin/env bash

# Destination directory have to be passed as first argument of script.
# Application binary path have to be passed as second argument.

Error () {
    echo "ERROR: $@" 1>&2
}

if [ -z "$1" ]; then
    Error "No binary path provided. XXX: $1"
    exit 1
fi

if ! [ -x "$1" ]  || ! [ -r "$1" ]; then
    Error "Application executable path is not valid"
    exit 2
fi

LDPATH=""

PROJECT_BUILD_DIR="`dirname "$1"`"
PROJECT_MAIN_BUILD_DIR="`dirname "$PROJECT_BUILD_DIR"`"

# Patterns used to exclude paths by dir names.
EXCLUDE_DIRS="build AppDir *.home"

# Enable recursive glob (may be unused here)
shopt -s globstar

# Iterate over directories in given path.
for i in "$PROJECT_MAIN_BUILD_DIR"/*/; do
    # Get all directories containing .so files and reduce them if repeating.
    for dir_path in $(find "$i" -name '*.so*' -printf '%h\n' | sort -u); do
        PATH_OK=1
        for d in $EXCLUDE_DIRS; do
            # Match path by directory name pattern
            if [[ "$dir_path" == *"/$d/"* ]]; then
                PATH_OK=0
            fi
        done
        # Append if n one of patterns matched
        if [ $PATH_OK -eq 1 ]; then
            LDPATH="$LDPATH:$dir_path"
        fi
    done
done

# Print LD_LIBRARY_PATH to STDOUT for script to get it.
echo "$LDPATH"
